import argparse
import itertools
import json
import sys
import pandas as pd
import urllib3
from http.client import responses
from types import SimpleNamespace
from functools import partial

class Attr:
    def __init__(self, name, value, iri):
        self.name = name
        self.value = value
        self.iri = iri
    def __str__(self):
        return "{}={}".format(self.name,self.value)

class Entry:
    def __init__(self, _id, attr, dblinks):
        self._id = _id
        self.attr = attr
        self.dblinks = dblinks

    def __str__(self):
        return "{}\n{}\n{}\n".format(self._id,self.attr,self.dblinks)

class Db:
    def __init__(self, name, entries):
        self.name = name
        self.entries = entries

    def __str__(self):
        return "{}\n{}\n".format(self.name,self.entries)

class DbObject:
    def __init__(self, name, attrs):
        self.name = name
        self.attrs = attrs

def main(argv):
    parser = argparse.ArgumentParser(description='Set input, output and other parameters')
    parser.add_argument('-i', dest='input', default='', help='')
    parser.add_argument('-o', dest='out', default='', help='')
    args = parser.parse_args()
    return args.input, args.out

def flattenList(lst):
    return [item for sublist in lst for item in sublist]

def getTSVInput(input):
    df = pd.read_csv(input, sep='\t', engine='python')
    df.columns = df.columns.str.strip()
    return df

def getAttr(input, column_name):
    df = getTSVInput(input)
    res = df[column_name].tolist()
    attrList = [attr.strip() for attr in res ]
    return attrList

def createSimpleNamespace(url):
    http = urllib3.PoolManager()
    data = SimpleNamespace()
    with http.request("GET",url,preload_content=False) as req:
        httpStatus = req.status
        httpDesc = responses[httpStatus]
        if not (httpDesc == "OK"):
            e = "{}, {}".format(httpStatus,httpDesc)
            raise Exception(e)
        else:
            response = req.data
            data = json.loads(response,object_hook=lambda x: SimpleNamespace(**x))
    return data


def createEntry(data):#one entry per time
    #data is list of objects, each object has an _id and attrs list
    id = data.id
    attrNs = flattenList([list(ns.__dict__.items()) for ns in data.attrs])
    dblinks = []
    attrs = []
    for k, g in itertools.groupby(attrNs,lambda t: t[0]): #returns an iterator, can iterate only one time
        if k.startswith("dblink"):
            dblinks.append(list(g)) # will work since we iterate append only one time per iteration
        elif k != "id" and not k.startswith("_"):
            data = flattenList([v[1].obj for v in list(g)])
            vals = [ns.val for ns in data]
            iri = flattenList([ns.iri for ns in data])
            attrs.append(Attr(k,vals,iri))
    iris = flattenList([ns.iri for ns in flattenList([ns[1].obj for ns in flattenList(dblinks)])])
    return Entry(id,attrs,iris)

def createEntries(db,ver):
    urlBase = "https://databasesapi.sfb.uit.no/"
    urlRoute = "rest/v1/{db_name}/records?ver={version}".format(db_name = db.name,version = ver)
    params = "&page%5Bsize%5D=100000&page%5Boffset%5D=0&format=raw"
    url = urlBase + urlRoute + params
    data = createSimpleNamespace(url)
    records = [createEntry(e) for e in data]
    records = [e for e in records if e.dblinks != []]
    return records


def writeToFile(dbs,outputPath):
    objDict = lambda obj: obj.__dict__
    with open(outputPath,"w", encoding="UTF-8") as f:
        json.dump(dbs,f,default=objDict,indent=4,ensure_ascii=False)

def getJson(df):
    attrKeysValues = [df[column_name].tolist() for column_name in ["Database","Attribute"]]
    zipped = list(zip(attrKeysValues[0],attrKeysValues[1]))
    zipped.sort()
    grouped = itertools.groupby(zipped,lambda x: x[0])
    dbObjects = []
    for k, g in grouped:
        dKey = "ebp-nor_" + k.lower()
        dValues = []
        for t in g:
            dValues.append(t[1])
        dbObjects.append(DbObject(dKey,dValues))
    partialCreateEntries = partial(createEntries,ver="1.1") #returns list of records
    dbs = [Db(dbObj.name,partialCreateEntries(dbObj)) for dbObj in dbObjects]
    return dbs



if __name__ == "__main__":
    inputData, outputData = main(sys.argv[1:])
    df = getTSVInput(inputData)
    data = getJson(df)
    writeToFile(data,outputData)
